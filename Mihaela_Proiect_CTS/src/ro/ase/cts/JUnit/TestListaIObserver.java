package ro.ase.cts.JUnit;

import ro.ase.cts.NavaPasageri;
import ro.ase.cts.TurnDeControl;
import junit.framework.TestCase;

public class TestListaIObserver extends TestCase {
	
	//fixture
	TurnDeControl turndeControlA, turndeControlB;
	NavaPasageri navaPasageri;

	protected void setUp() throws Exception {
		turndeControlA = new TurnDeControl("A");
		turndeControlB = new TurnDeControl("B");
		navaPasageri = new NavaPasageri("Nicodisar", "James ", 600, 452, 2600,300,true,false,false,true);
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testAdaugaObservatorv(){
		try{
			navaPasageri.addObserver(turndeControlA);
			assertEquals("Test adaugare observator in lista:",1,navaPasageri.getObservatoriList().size());
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	public void testAdaugaObservatorNull(){
		TurnDeControl turndeControl = null;
		try{
			navaPasageri.addObserver(turndeControl);
			fail("Observatorul adaugat are val null!");
				
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void testStergeObservator(){
		try{
			navaPasageri.removeObserver(turndeControlA);
			assertEquals("Test stergere observator din lista", 0,navaPasageri.getObservatoriList().size());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void testStergeObservatorDacaListaESTEGoala(){
		try{
			if(navaPasageri.getObservatoriList().size() ==0){
				navaPasageri.removeObserver(turndeControlA);
				fail("Se accepta stergerea dintr-o lista goala!");
			}else{
				throw new Exception("Lista nu este goala!");
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	

}
