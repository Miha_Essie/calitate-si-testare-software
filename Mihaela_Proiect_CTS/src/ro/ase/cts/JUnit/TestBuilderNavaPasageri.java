package ro.ase.cts.JUnit;

import ro.ase.cts.BuilderNavaPasageri;
import ro.ase.cts.NavaPasageri;
import ro.ase.cts.Enum.TipNava;
import junit.framework.TestCase;

public class TestBuilderNavaPasageri extends TestCase {
	
	NavaPasageri navaPasageri;
	public static final String numeNavaInitial = "Queen Mary";
	public static final String numeCapitanInitial = "Jhon";
	public static final double greutateInitiala = 500.00;
	public static final int dimensiuneInitiala = 250;
	public static final int nrPasageriInitial = 2000;
	public static final int nrBarciSalvareInitial = 500;
	public static final int minNrPasageri = 50;
	public static final int maxNrpasageri =3000;

	protected void setUp() throws Exception {
		System.out.println("Pregatire test builder Nava pasageri.");
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testBuilderValoriNormale(){
		try{
			navaPasageri = new BuilderNavaPasageri(TipNava.PASAGERI, numeNavaInitial, numeCapitanInitial, greutateInitiala, dimensiuneInitiala, nrPasageriInitial, nrBarciSalvareInitial).build();
			assertEquals("Verificare constructor tip pasageri",TipNava.PASAGERI,navaPasageri.getTip());
			assertEquals("Verificare numeNava", numeNavaInitial,navaPasageri.getNumeNava());
			assertEquals("Verificare numeCapitan",numeCapitanInitial, navaPasageri.getNumeCapitan());
			assertEquals("Verificare greutate nava:", greutateInitiala,navaPasageri.getGreutateNava());
			assertEquals("Verificare dimensiune nava",dimensiuneInitiala,navaPasageri.getDimensiuneNava());
			assertEquals("Verificare nrMaximPasageri nava",nrPasageriInitial, navaPasageri.getNrMaximPasageri());
			assertEquals("Verificare nrBarciSalvare nava",nrBarciSalvareInitial,navaPasageri.getNrBarciSalvare());
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void testBuilderValoriNegativePtNrPasageri(){
		int nrPasageri = -5;
		try{
			navaPasageri = new BuilderNavaPasageri(TipNava.PASAGERI, numeNavaInitial, numeCapitanInitial, greutateInitiala, dimensiuneInitiala, nrPasageri, nrBarciSalvareInitial).build();
			fail("Nr de pasageri accepta valori negative!");
		}catch(Exception e){
			e.printStackTrace();
		}	
	}
	
	public void testBuilderValoriMaiMareCa3000(){
		int nrPasageri = 3500;
		try{
		navaPasageri = new BuilderNavaPasageri(TipNava.PASAGERI, numeNavaInitial, numeCapitanInitial, greutateInitiala, dimensiuneInitiala, nrPasageri, nrBarciSalvareInitial).build();
		fail("Nr de pasageri accepta valori mai mari ca nrMaxim!");
		}catch(Exception e){e.printStackTrace();}
		}
		
	
	public void testBuilderValoareMinPtNrPasageri(){
		try{
			navaPasageri = new BuilderNavaPasageri(TipNava.PASAGERI, numeNavaInitial, numeCapitanInitial, greutateInitiala, dimensiuneInitiala, minNrPasageri, nrBarciSalvareInitial).build();
			assertEquals("Verificare val minima pt nr de pasageri", minNrPasageri,navaPasageri.getNrMaximPasageri());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void testBuilderValoareMaximaPtNrPasageri(){
		try{
			navaPasageri = new BuilderNavaPasageri(TipNava.PASAGERI, numeNavaInitial, numeCapitanInitial, greutateInitiala, dimensiuneInitiala, maxNrpasageri, nrBarciSalvareInitial).build();
			assertEquals("Verificare val maxima pt nr de pasageri", maxNrpasageri,navaPasageri.getNrMaximPasageri());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
}
