package ro.ase.cts.JUnit;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite(AllTests.class.getName());
		//$JUnit-BEGIN$
		suite.addTestSuite(TestConstructorNava.class);
		suite.addTestSuite(TestTipNava.class);
		suite.addTestSuite(TestBuilderNavaPasageri.class);
		suite.addTestSuite(TestDestinatie.class);
		suite.addTestSuite(TestListaIObserver.class);
		//$JUnit-END$
		return suite;
	}

}
