package ro.ase.cts.JUnit;

import ro.ase.cts.FactoryNava;
import ro.ase.cts.Nava;
import ro.ase.cts.Enum.TipNava;
import junit.framework.TestCase;

public class TestTipNava extends TestCase {
	
	//fixture
	FactoryNava factoryNava;
	Nava nava;
	public static final String numeNavaInitial = "Queen Mary";
	public static final String numeCapitanInitial = "Jhon";
	public static final double greutateInitiala = 500.00;
	public static final int dimensiuneInitiala = 250;
	
	

	protected void setUp() throws Exception {
		System.out.println("Pregatire test constructor nava.");
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	
	public void testGetFactoryValoriNormale(){
		try{
			nava=factoryNava.getNava(TipNava.PASAGERI);
			assertEquals("Verificare tip bilet: ",TipNava.PASAGERI,nava.getTip());
		}catch(Exception e){
			e.printStackTrace();
		}	
	}
	public void testGetFactoryTipNull(){
		try{
			nava = factoryNava.getNava(null);
			assertNotNull(nava.getTip());
			
		}catch(Exception e){
			e.printStackTrace();
		}	
		
	}
}
