package ro.ase.cts.JUnit;

import ro.ase.cts.NavaPasageri;
import ro.ase.cts.NavaTransport;
import junit.framework.TestCase;

public class TestDestinatie extends TestCase {
	
	//fixture
	NavaTransport navaTransport;
	NavaPasageri navaPasageri;
	public static final String destinatieInitiala = "Hamburg";
	

	protected void setUp() throws Exception {
		
		navaTransport = new NavaTransport("Seawise Giant", "Alberto ", 453.012, 458, 657.019, "petrol");
		navaPasageri = new NavaPasageri("Nicodisar", "James ", 600, 452, 2600, 300, false, true, true, true);
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testNavaTransportDestinatieValoareNormala(){
		try{
			navaTransport.navigheza(destinatieInitiala);
					
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void testNavaPasageriDestinatieValoareNormala(){
		try{
			navaPasageri.navigheza(destinatieInitiala);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void testNavaTransportDestinatieNull(){
		String destinatie=null;
		try{
			navaTransport.navigheza(destinatie);
			fail("Destinatia accepta null -nava Transport !");
		}catch(Exception e){
			e.printStackTrace();
			
		}
	}
	
	public void testNavaPasageriDestinatieNull(){
		String destinatie = null;
		try{
			navaPasageri.navigheza(destinatie);
			fail("Destinatia accepta null -nava Pasageri !");
			
		}catch(Exception e){
			e.printStackTrace();
			
		}
	}
	
	public void testNavaTransportDestinatieEmptyString(){
		String destinatie ="";
		try{
			navaTransport.navigheza(destinatie);
			fail("Destinatia accepta empty String -nava Transport !");
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void testNavaPasageriDestinatieEmptyString(){
		String destinatie = "";
		try{
			navaPasageri.navigheza(destinatie);
			fail("Destinatia accepta empty String -nava Pasageri !");
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
}
