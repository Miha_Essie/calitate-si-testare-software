package ro.ase.cts.JUnit;

import ro.ase.cts.Nava;
import ro.ase.cts.NavaPasageri;
import junit.framework.TestCase;

public class TestConstructorNava extends TestCase {
	
	//fixture
	NavaPasageri nava;
	public static final String numeNavaInitial = "Queen Mary";
	public static final String numeCapitanInitial = "Jhon";
	public static final double greutateInitiala = 500.00;
	public static final int dimensiuneInitiala = 250;
	

	protected void setUp() throws Exception {
		System.out.println("Pregatire test constructor nava.");
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testConstructorValoriNormale(){
		nava = new NavaPasageri(numeNavaInitial,numeCapitanInitial,greutateInitiala,dimensiuneInitiala);
		assertEquals("Verificare constructor numeNava", numeNavaInitial, nava.getNumeNava());
		assertEquals("Verificare constructor numeCapitan", numeCapitanInitial, nava.getNumeCapitan());
		assertEquals("Verificare constructor greutate", greutateInitiala, nava.getGreutateNava());
		assertEquals("Verificare constructor dimensiune", dimensiuneInitiala, nava.getDimensiuneNava());
		
	}

	public void testConstructorNumeNull(){
		String nume = null;
		try{
			nava = new NavaPasageri(nume, numeCapitanInitial, greutateInitiala,dimensiuneInitiala);
			fail("Lipsa exceptie nume=null");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void testConstructorNumeCapitanNull(){
		String numeCapitan = null;
		try{
			nava = new NavaPasageri(numeNavaInitial, numeCapitan, greutateInitiala, dimensiuneInitiala);
			fail("Lipsa exceptie nume=null");
		}catch(Exception e){
			e.printStackTrace();
			}
	}
	
	public void testConstructorGreutateNegativa(){
		double greutate = -52.0;
		try{
			nava = new NavaPasageri(numeNavaInitial, numeCapitanInitial, greutate, dimensiuneInitiala);
			fail("Greutatea navei are valoare negativa");
		}catch(Exception e){
			e.printStackTrace();
			}
	}
	public void testConstructorDimensiunenegativa(){
		int dimensiune = -200;
		try{
			nava = new NavaPasageri(numeNavaInitial, numeCapitanInitial, greutateInitiala, dimensiune);
			fail("Dimensiunea navei accepta valori negative");
		}catch(Exception e){
			e.printStackTrace();
			}
	}
	
}
